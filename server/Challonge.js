// var apiKey = "goo3EJ6VxxMi0BOPWSxLP4bVZ0kExn4k4uAG57YV";

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'
          ? args[number]
          : match
          ;
    });
  };
}

function updateMatchList(error, resp) {
  var tMatches = resp.data;

  //handle the case where we only get a single match as the response
  if (typeof tMatches == "object") {
    var mat = tMatches.match;
    mat._id = mat.id + "";
    Winston.info(i + " updating par", Matches.upsert({_id: mat._id}, mat));
    return;
  }
  if (error != null) {
    Winston.info("error? " + error);
  }
  if (tMatches != null) {

    Winston.info("here's the length of tMatches: " + tMatches.length);
    for (var i = 0; i < tMatches.length; i++) {
      var mat = tMatches[i].match;
      mat._id = mat.id + "";
      Winston.info(i + " updating par", Matches.upsert({_id: mat._id}, mat));

    }
  } else {
    Winston.info("tMatches was null");
  }
}

var tournamentUrl = "https://api.challonge.com/v1/tournaments/{0}.json";
var matchUrl = "https://api.challonge.com/v1/tournaments/{0}/matches.json";
var updateUrl = "https://api.challonge.com/v1/tournaments/{0}/matches/{1}.json";

Meteor.methods({
  loadMatches: function (id) {
    check(id, String);

    var url = matchUrl.format(id);
    Winston.info("Ch url", url);

    HTTP.get(url,
        {
          params: {
            api_key: Meteor.user().profile.apiKey,
            // state: "open" // in the future (default is all...)
            //participant_id: {id}  // not going to use it but its available...
          }
        },
        updateMatchList
    );
    return "done";

  },
  updateMatch: function (tournamentID, matchID, winner, score) {
    var url = updateUrl.format(tournamentID, matchID);
    Winston.info("updating match id: " + matchID + " tid is " + tournamentID);
    Winston.info("url is ", url);
    HTTP.put(url,
        {
          params: {
            api_key: Meteor.user().profile.apiKey,
            "match[scores_csv]": score,
            "match[winner_id]": winner
          }
        },
        updateMatchList
    );
  },
  loadTournament: function (id) {
    check(id, String);

    var url = tournamentUrl.format(id);
    Winston.info("Ch url", url);
    HTTP.get(url,
        {
          params: {
            api_key: Meteor.user().profile.apiKey,
            include_participants: 1
          }
        },
        function (error, resp) {
          var t = resp.data.tournament;
          t._id = t.id + "";
          var tParticipants = t.participants;
          Winston.info("Found " + tParticipants.length + " participants for tournament " + t._id);
          delete t.participants;

          Tournaments.upsert({_id: t._id}, t);
          for (var i = 0; i < tParticipants.length; i++) {
            var par = tParticipants[i].participant;
            Winston.info("Found participant ", par);
            par._id = par.id + "";
            Winston.info("updating par", Participants.upsert({_id: par._id}, par));
          }
        }
    );
    return "done";
  },

  getUsersTournaments: function () {
    var url = "https://api.challonge.com/v1/tournaments.json";

    HTTP.get(url,
        {
          params: {
            api_key: Meteor.user().profile.apiKey
          }
        },
        function (error, resp) {
          var size = resp.data.length;
          for (var i = 0; i < size; i++) {
            var t = resp.data[i].tournament;
            t._id = t.id + "";

            Tournaments.upsert({_id: t._id}, t);
          }
        }
    );
    return "done";
  }
});
