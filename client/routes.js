angular.module("challenge").run(["$rootScope", "$state", function ($rootScope, $state) {
  $rootScope.$on("$stateChangeError", function (event, next, previous, error) {
    // We can catch the error thrown when the $meteor.requireUser() promise is rejected
    // and redirect the user back to the login page
    if (error === "AUTH_REQUIRED") {
      // It is better to use $state instead of $location. See Issue #283.
      $state.go('login');
    }
  });
}]);

angular.module("challenge").config(['$urlRouterProvider', '$stateProvider', '$locationProvider',
  function ($urlRouterProvider, $stateProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    var loginResolver = {
      "currentUser": ["$meteor", function ($meteor) {
        return $meteor.requireUser();
      }]
    };

    $stateProvider
      .state('participants', {
        url: '/tournament/:id/participants',
        templateUrl: 'client/participants/views/participants-list.ng.html',
        controller: 'PartiesListCtrl',
        resolve: loginResolver
      })
      .state('tournament', {
        url: '/tournament/:id',
        templateUrl: 'client/tournament/view/tournament.ng.html',
        controller: 'TournamentController',
        resolve: loginResolver
      })
      .state('login', {
        url: '/login',
        templateUrl: 'client/login/views/login.ng.html',
        controller: 'LoginController'
      })
      .state('register', {
        url: '/register',
        templateUrl: 'client/login/views/register.ng.html',
        controller: 'RegisterController'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'client/home/views/home.ng.html',
        controller: 'HomeController',
        resolve: loginResolver
      })
      .state('score', {
        url: '/score/:matchID/:tournamentID',
        templateUrl: 'client/score/views/score.ng.html',
        controller: 'ScoreController',
        resolve: loginResolver
      })
      .state('stations', {
        url: '/tournament/:id/stations',
        templateUrl: 'client/stations/view/stations.ng.html',
        controller: 'StationsController',
        resolve: loginResolver
      })
      .state('assign-station', {
        url: '/match/:matchID/assign-station',
        templateUrl: 'client/assign-station/view/assign-station.ng.html',
        controller: 'AssignStationController',
        resolve: loginResolver
      })
    ;

    $urlRouterProvider.otherwise("/home");
  }]);
