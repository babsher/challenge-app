angular.module('challenge').controller('RegisterController', ['$scope', '$meteor', '$state',
  function ($scope, $meteor, $state) {
    $scope.runReg = function () {
      $meteor.createUser({
        email: $scope.user,
        password: $scope.password,
        profile: {
          apiKey: $scope.apiKey
        },
        registration: 'unverified'
      }).then(function () {
        console.log('Login success');
        $state.go("home")
      }, function (err) {
        console.log('Login error - ', err);
      });
    }
  }]);