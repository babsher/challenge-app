angular.module('challenge').controller('LoginController', ['$scope', '$meteor', '$state', '$rootScope',
  function($scope, $meteor, $state, $root) {
    $scope.login = function() {
      $meteor.loginWithPassword($scope.user, $scope.password).then(function(){
        console.log('Login success');
        $state.go("home");
      }, function(err){
        console.log('Login error - ', err);
      });
    };

    $scope.register = function() {
      $state.go("register")
    };
  }]);