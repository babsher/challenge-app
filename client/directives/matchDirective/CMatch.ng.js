angular.module('challenge')
  .directive('cMatch', function(matchesService) {

    var controller = function($scope, $mdDialog, matchesService){
      var originatorEv;
      $scope.openMenu = function($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
      };

      (function() {
        if(typeof $scope.match === 'object') {
          $scope.m = $scope.match
        } else if (typeof $scope.match === 'string' || typeof $scope.match == 'number') {
          $scope.m = matchesService.get($scope.match);
        } else {
          console.log('Error type not right', $scope.match);
        }
      })();
    };

    return {
      scope: {
        match: '=',
        menu: '='
      },
      templateUrl: 'client/directives/matchDirective/c-match.ng.html',
      controller: controller
    };
  });