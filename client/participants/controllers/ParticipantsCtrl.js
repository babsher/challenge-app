angular.module("challenge").controller("PartiesListCtrl",
  ['$scope', '$meteor', '$rootScope', '$state', '$stateParams',
  function($scope, $meteor, $rootScope, $state, $stateParams){
    $scope.t = $meteor.object(Tournaments, $stateParams.id);

    $scope.participants = $meteor.collection(function(){
      return Participants.find({tournament_id: parseInt($stateParams.id)});
    });
  }]);
