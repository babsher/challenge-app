angular.module('challenge').controller('HomeController', ['$scope', '$meteor', '$state', '$rootScope',
  function($scope, $meteor, $state, $root) {

    $scope.tours = $meteor.collection(Tournaments);

    $scope.loadTournament = function() {
      $meteor.call('getUsersTournaments')
    };

    $scope.go = function(params) {
      $state.go('tournament', params);
    }
  }]);