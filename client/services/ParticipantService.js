angular.module("challenge").factory('participantService',
  function(){
    return {
      get: function(id) {
        return Participants.findOne({'_id': id});
      },

      getName: function(id) {
        id = id + '';
        var p = Participants.findOne({'_id': id});
        if(p) {
          return p.name;
        } else {
          return 'Unknown Player:' + id;
        }
      }
    }
});