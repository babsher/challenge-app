angular.module("challenge").factory('matchesService',
  function($meteor){
    return {
      get: function(id) {
        return Matches.findOne({'_id': id + ''});
      },

      forTournament: function(id) {
        return $meteor.collection(function () {
          return Matches.find({tournament_id: parseInt(id)});
        });
      },

      rounds: function(matches) {
        var rounds = [];
        for (var i = 0; i < matches.length; i++) {
          var r = matches[i].round;
          if (rounds.indexOf(r) == -1) {
            rounds.push(r)
          }
        }
        return rounds;
      }
    }
});