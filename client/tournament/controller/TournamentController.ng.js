angular.module('challenge').controller('TournamentController',
    function ($scope, $meteor, $state, $stateParams, matchesService) {
      $scope.tournament = $meteor.object(Tournaments, $stateParams.id);

      $scope.matches = matchesService.forTournament($stateParams.id);

      var rounds = matchesService.rounds($scope.matches);

      rounds.sort(function (a, b) {
        return Math.abs(a) - Math.abs(b)
      });

      for (i = 0; i < rounds.length; i++) {
        if (rounds[i] < 0) {
          rounds[i] = {
            id: rounds[i],
            name: "Losers round: " + Math.abs(rounds[i])
          };
        } else {
          rounds[i] = {
            id: rounds[i],
            name: "Winners round: " + rounds[i]
          }
        }
      }
      $scope.rounds = rounds;

      // hide completed
      $scope.matchFilter = {
        state: '!complete'
      };

      $scope.loadMatches = function () {
        $meteor.call('loadTournament', $stateParams.id);
        $meteor.call('loadMatches', $stateParams.id);
      };

      $scope.assignStation = function (match) {
        console.log(match);
      };

      $scope.stations = $meteor.collection(function() {
        return Stations.find({tournament_id: $stateParams.id + ''});
      });
    });
