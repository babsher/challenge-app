angular.module("challenge").filter('matchesFilter',
  ['matchesService',
    function(matchesService) {
      return function(input) {
        if(input !== false) {
          return matchesService.get(input);
        } else {
          return '';
        }
      };
    }]);