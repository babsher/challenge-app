angular.module("challenge").filter('participantName',
  ['participantService',
    function(ParticipantService) {
      return function(input) {
        if(input !== false) {
          return ParticipantService.getName(input);
        } else {
          return '';
        }
      };
  }]);