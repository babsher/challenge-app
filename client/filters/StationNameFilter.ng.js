angular.module("challenge").filter('stationName',
    function (stationsService) {
      return function (input) {
        if (input) {
          if(typeof input == 'object') {
            return 'Station ' + input.name;
          } else {
            return 'Station ' + stationsService.get(input).name;
          }
        } else {
          return 'Unknown Station';
        }
      };
    });