angular.module('challenge').controller('StationsController',
  function($scope, $meteor, $stateParams) {
    $scope.tournament = $meteor.object(Tournaments, $stateParams.id);

    $scope.addStation = function() {
      Stations.insert({name: $scope.station, tournament_id: $stateParams.id});
    };

    $scope.stations = $meteor.collection(function(){
      return Stations.find({tournament_id: $stateParams.id});
    });

    $scope.removeStation = function(stationID) {
      Stations.remove({_id: stationID});
    };

  });


