angular.module('challenge').controller('AssignStationController',
  function($scope, $meteor, $stateParams) {
    $scope.match = $meteor.object(Matches, $stateParams.matchID);
    $scope.tournament = $meteor.object(Tournaments, $scope.match.tournament_id + "");

    $scope.addStation = function() {
      Stations.insert({name: $scope.station, tournament_id: $scope.match.tournament_id + ""});
    };

    $scope.stations = $meteor.collection(function(){
      return Stations.find({match_id: { $exists: false}, tournament_id: $scope.match.tournament_id + ""});
    });
  });
