angular.module("challenge").controller("ScoreController",
  function($scope, $meteor, $rootScope, $state, $stateParams){

    $scope.stateParams = $stateParams;

    $scope.score = $meteor.object(Matches, ""+$stateParams.matchID);

    $scope.incrementScore = function(id){
      if(id == 1){
        $scope.player1Score++;
      }
      else{
        $scope.player2Score++;
      }
    }
    $scope.decrementScore = function(id){
      if(id == 1){
        if( $scope.player1Score > 0){
          $scope.player1Score--;
        }
      }
      else{
        if( $scope.player2Score > 0){
          $scope.player2Score--;
        }
      }
    }
    $scope.updateScore = function(){ //TODO: not considering ties
        var winner_id = -1;
        if($scope.player1Score>$scope.player2Score){
          winner_id = $scope.score.player1_id
        }else{
          winner_id = $scope.score.player2_id
        }
        var _score = $scope.player1Score + "-" + $scope.player2Score;

        $meteor.call('updateMatch', $stateParams.tournamentID, $stateParams.matchID, winner_id, _score);
    }
    $scope.player1Score = 0;
    $scope.player2Score = 0;

  });
